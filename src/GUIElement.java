import org.lwjgl.examples.spaceinvaders.Sprite;

public interface GUIElement {
    int getWidth();

    int getHight();

    int getY();

    int getX();


    Sprite getSprite();

    int reciveClick(int x, int y, int button);

    default boolean isHit(int xclick, int yclick) {
        return ((xclick > getX()) && (xclick < getX() + this.getWidth())) && ((yclick > getY()) && (yclick < getY() + this.getHight()));
    }


}